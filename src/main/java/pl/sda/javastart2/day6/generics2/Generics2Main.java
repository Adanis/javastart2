package pl.sda.javastart2.day6.generics2;

import pl.sda.javastart2.day6.generics1.Apple;
import pl.sda.javastart2.day6.generics1.Orange;

public class Generics2Main {
    public static void main(String[] args) {
        firstGenericExample();

    }

    private static void firstGenericExample() {
        GenericFruitBox<Orange> orangeBox = new GenericFruitBox<>(new Orange());
        GenericFruitBox<Apple> appleBox = new GenericFruitBox<>(new Apple());
    }


}
