package pl.sda.javastart2.day6.enums;

import java.util.Arrays;

public class EnumExampleMain {
    public static void main(String[] args) {
        Car whiteCar = new Car(ColourEnum.WHITE);
        Car blackCar = new Car(ColourEnum.BLACK);
        Car anotherBlackCar = new Car(ColourEnum.BLACK);

        System.out.println(anotherBlackCar.equals(blackCar));
        System.out.println(whiteCar.equals(blackCar));

        ColourEnum[] values = ColourEnum.values();
        System.out.println(Arrays.toString(values));

        Car newCar = null;
        for (ColourEnum colourEnum : ColourEnum.values()){
            if(colourEnum.getNumberValue()==0){
                newCar = new Car(colourEnum);
                break;
            }
        }
    }
}
