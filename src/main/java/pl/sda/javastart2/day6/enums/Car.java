package pl.sda.javastart2.day6.enums;

public class Car {
    private ColourEnum colour;

    public Car(ColourEnum colour) {
        this.colour = colour;
    }

        @Override
        public boolean equals(Object obj) {
            return colour.equals(((Car) obj).getColour());
        }

    public ColourEnum getColour() {
        return colour;
    }
}
