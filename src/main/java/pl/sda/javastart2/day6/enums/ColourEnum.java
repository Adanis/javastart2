package pl.sda.javastart2.day6.enums;

public enum ColourEnum {
    WHITE(0,"biały"),BLACK(1,"czarny"),RED(2,"czerwony"),GREEN(3,"zielony"),GOLD(4,"złoty"),SILVER(5,"srebrny"),BLUE(6,"niebieski");
    private int numberValue;
    private String plName;

    ColourEnum(int numberValue, String plName) {
        this.numberValue = numberValue;
        this.plName = plName;
    }

    public int getNumberValue() {
        return numberValue;
    }

    public String getPlName() {
        return plName;
    }
}
