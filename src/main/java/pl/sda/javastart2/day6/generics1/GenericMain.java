package pl.sda.javastart2.day6.generics1;

public class GenericMain {
    public static void main(String[] args) {
        firstStepGenerics();
        secondStepGenerics();


    }

    private static void firstStepGenerics() {
        OrangeBox orangeBox = new OrangeBox(new Orange());
        AppleBox appleBox = new AppleBox(new Apple());
    }

    private static void secondStepGenerics() {
        FruitBox fruitBoxOfApples = new FruitBox(new Apple());
        FruitBox fruitBoxOfOranges = new FruitBox(new Orange());
        Fruit fruitApple = fruitBoxOfApples.getFruit();
        Fruit fruitOrange = fruitBoxOfOranges.getFruit();
    }
}

