package pl.sda.javastart2.day8.files;

import java.io.*;

public class FileOperations {
    private static String filePath = "C:\\Projects\\pan.txt";

    public static void main(String[] args) {

//        rewriteFilesNewWay();
//        readFileWithBufferedReader();


    }

    private static void readFileWithBufferedReader() {
        try(
        FileReader fileReader = new FileReader(filePath);
        ) {
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = bufferedReader.readLine();
                System.out.println(line);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    private static void rewriteFilesNewWay() {
        try(
        FileInputStream fileInputStream = new FileInputStream(filePath);
        FileOutputStream fileOutputStream = new FileOutputStream(filePath.replace("pan.txt", "panKopia.txt"));
        ) {
            int b;
            while((b=fileInputStream.read())!=-1){
                fileOutputStream.write(b);
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
