package pl.sda.javastart2.day8;

public class ThreadsExample {
    public static void main(String[] args) {
        runnableBasics();
    }

    private static void runnableBasics() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Anonimowa klasa zagniezdzona");
            }
        };

        Runnable lambda = () -> System.out.println("lambda");
        Runnable ourRunnable = new OurRunnable();
    }


}
