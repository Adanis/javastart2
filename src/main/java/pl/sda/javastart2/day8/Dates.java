package pl.sda.javastart2.day8;

import java.time.*;
import java.util.Date;

public class Dates {
    public static void main(String[] args) {
        instant();
        duration();
        localDateTime();
    }

    private static void localDateTime() {
        LocalDateTime now = LocalDateTime.now();
        Date date = new Date();
        System.out.println("LocalDateTime" + now);
        System.out.println("Date" + date);

        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();

        LocalDateTime.of(localDate, localTime);


    }

    private static void duration() {
        Duration duration30days = Duration.ofDays(30);
        System.out.println("30 days = " + duration30days.getSeconds());
    }

    private static void instant() {
        Instant now = Instant.now();
        System.out.println("Current timestamp: " + now);

        Instant epochMilli = Instant.ofEpochMilli(3423644532245L);
        System.out.println("Specific time from epoch: " + epochMilli);
    }
}
