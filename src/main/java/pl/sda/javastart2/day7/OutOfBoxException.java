package pl.sda.javastart2.day7;

public class OutOfBoxException extends Exception {
    public OutOfBoxException(String message) {
        super(message);
    }
}