package pl.sda.javastart2.day7;

public class NegativeNumberException extends RuntimeException{

    public NegativeNumberException(String message) {
        super(message);
    }
}
