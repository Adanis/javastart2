package pl.sda.javastart2.day5;

import java.util.HashSet;
import java.util.Set;

public class HashSetExample {
    public static void main(String[] args) {
        basicHashSetOperations();

        addingPersonToHashSet();



    }

    private static void addingPersonToHashSet() {
        Person tomek = new Person(123, "Tomek");
        Person ania = new Person(123, "Ania");
        Person ola = new Person(253, "Ola");
        Person magda = new Person(153, "Magda");
        Person magda2 = new Person(153, "Magda");

        HashSet<Person> people = new HashSet<>();
        people.add(tomek);
        people.add(ania);
        people.add(ola);
        people.add(magda);
        people.add(magda2);

        System.out.println(people);
    }

    private static void basicHashSetOperations() {
        Set<String> firstSet = new HashSet<>();
        firstSet.add("5");
        firstSet.add("6");
        firstSet.add("2");
        firstSet.add("3");
        boolean added1 = firstSet.add("5");
        boolean added2 = firstSet.add("9");
        boolean added3 = firstSet.add("2");
        System.out.println(added1+", "+added2+", "+added3);
        System.out.println("Nasz set: "+firstSet);
        System.out.println("set.size: "+firstSet.size());
        System.out.println("set.contains(\"2\"): "+firstSet.contains("2"));
    }
}
