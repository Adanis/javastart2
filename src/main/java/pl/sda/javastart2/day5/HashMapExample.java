package pl.sda.javastart2.day5;

import java.util.HashMap;

public class HashMapExample {

    public static void main(String[] args) {
        basicMapOperations();
    }

    private static void basicMapOperations() {
        Person tomek = new Person(123, "Tomek");
        Person ania = new Person(234, "Ania");
        Person ola = new Person(345, "Ola");
        Person ola2 = new Person(456, "Ola");

        HashMap<Integer, Person> peopleByPesel = new HashMap<>();

        peopleByPesel.put(tomek.getPesel(), tomek);
        peopleByPesel.put(ania.getPesel(), ania);
        peopleByPesel.put(ola.getPesel(), ola);
        peopleByPesel.put(ola2.getPesel(), ola2);

        System.out.println(peopleByPesel);

        properWayOfAddingPeople(tomek, ania, ola, ola2);
    }

    private static void properWayOfAddingPeople(Person tomek, Person ania, Person ola, Person ola2) {
        HashMap<String, Person> peopleByName = new HashMap<>();

        peopleByName.put(tomek.getName(), tomek);
        peopleByName.put(ania.getName(), ania);
        peopleByName.put(ola.getName(), ola);
        peopleByName.put(ola2.getName(), ola2);

        System.out.println(peopleByName);
    }
}
