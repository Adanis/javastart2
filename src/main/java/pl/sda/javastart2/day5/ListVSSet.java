package pl.sda.javastart2.day5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListVSSet {
    public static void main(String[] args) {
//        removeAllFromListAndSet();
        checkIfCollectionContains();
    }

    private static void checkIfCollectionContains() {
        List<Book> arrayList = new ArrayList<>();
        Set<Book> hashSet = new HashSet<>();
        int howManyBooks = 500_000;
        for (int i = 0; i < howManyBooks; i++) {
            arrayList.add(new Book(i));
            hashSet.add(new Book(i));
        }
        arrayList.add(new Book(500_000));

        long start = System.currentTimeMillis();
        arrayList.contains(new Book(howManyBooks));
        long finish = System.currentTimeMillis();
        System.out.println("Czas wykonania "+arrayList.getClass().getSimpleName()+ ": " +(finish-start)+" ms");

        long start2 = System.currentTimeMillis();
        hashSet.contains(new Book(howManyBooks));
        long finish2 = System.currentTimeMillis();
        System.out.println("Czas wykonania "+hashSet.getClass().getSimpleName()+ ": " +(finish2-start2)+" ms");
    }

    private static void removeAllFromListAndSet() {
        List<Book> arrayList = new ArrayList<>();
        Set<Book> hashSet = new HashSet<>();
        int howManyBooks = 500_000;
        for (int i = 0; i < howManyBooks; i++) {
            arrayList.add(new Book(i));
            hashSet.add(new Book(i));
        }
        arrayList.add(new Book(500_000));

        long start = System.currentTimeMillis();
        arrayList.removeAll(hashSet);
        long finish = System.currentTimeMillis();
        System.out.println("Czas wykonania "+arrayList.getClass().getSimpleName()+ ": " +(finish-start)+" ms");

        long start2 = System.currentTimeMillis();
        hashSet.removeAll(arrayList);
        long finish2 = System.currentTimeMillis();
        System.out.println("Czas wykonania "+hashSet.getClass().getSimpleName()+ ": " +(finish2-start2)+" ms");
    }

}
