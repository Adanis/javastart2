package pl.sda.javastart2.day5;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ArrayListVSLinkedList {
    private static List<Integer> arrayList = new ArrayList<>();
    private static List<Integer> linkedList = new LinkedList<>();

    public static void main(String[] args) {
        arrayListComparedToLinkedList();
    }

    private static void arrayListComparedToLinkedList() {
        int howManyTimes = 50_000;

        addingALotOfFieldsAtTheEnd(howManyTimes, arrayList);
        addingALotOfFieldsAtTheEnd(howManyTimes, linkedList);
        addingALotOfFivesAtTheStart(howManyTimes, arrayList);
        addingALotOfFivesAtTheStart(howManyTimes, linkedList);
        addingAllElements(arrayList);
        addingAllElements(linkedList);


    }

    private static void addingAllElements(List<Integer> list) {
        long start = System.currentTimeMillis();
        int i = 0;
        int sum = 0;
        while(i<list.size()){
            sum += list.get(i);
            i++;
        }

        long finish = System.currentTimeMillis();
        System.out.println("Czas wykonania "+list.getClass().getSimpleName()+ ": " +(finish-start)+" ms");
    }

    private static void addingALotOfFivesAtTheStart(int howManyTimes, List<Integer> list) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < howManyTimes ; i++) {
            list.add(0,5);
        }
        long finish = System.currentTimeMillis();
        System.out.println("Czas wykonania "+list.getClass().getSimpleName()+ ": " +(finish-start)+" ms");
    }


    private static void addingALotOfFieldsAtTheEnd(int howManyTimes, List<Integer> list) {
        long start = System.currentTimeMillis();
        while(list.size()<howManyTimes){
            list.add(5);
        }
        long finish = System.currentTimeMillis();
        System.out.println("Czas wykonania "+list.getClass().getSimpleName()+ ": " +(finish-start)+" ms");
    }
}
