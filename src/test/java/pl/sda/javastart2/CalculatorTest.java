package pl.sda.javastart2;

import org.junit.jupiter.api.Test;
import pl.sda.javastart2.day6.Calculator;

class CalculatorTest {

    @Test
    void shouldAddCorrectly() {
        int sum = Calculator.add(1,2);

        assert sum == 3;
    }

    @Test
    void testSleepIn() {
        boolean b1 = Calculator.sleepIn(false,false);
        boolean b2 = Calculator.sleepIn(true,false);
        boolean b3 = Calculator.sleepIn(false,true);


    }

}